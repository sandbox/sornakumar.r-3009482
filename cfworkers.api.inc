<?php

/**
 * @file
 * Provides API calls for cloudflare script, workers and routes.
 */

function cfworkers_api_call($url, $data, $method, $headers = FALSE, $data_type = '') {

  $cf_settings = _cloudflare_settings();

  if (is_array($headers)) {

    if (!isset($headers['X-Auth-Email']) || strlen($headers['X-Auth-Email']) == 0)
      $headers['X-Auth-Email'] = $cf_settings['cf_api_email'];

    if (!isset($headers['X-Auth-Key']) || strlen($headers['X-Auth-Key']) == 0)
      $headers['X-Auth-Key'] = $cf_settings['cf_api_key'];

    if (!isset($headers['Host']) || strlen($headers['Host']) == 0)
      $headers['Host'] = 'api.cloudflare.com';

    if (!isset($headers['Connection']) || strlen($headers['Connection']) == 0)
      $headers['Connection'] = 'Close';

    if (!isset($headers['Content-Type']) || strlen($headers['Content-Type']) == 0)
      $headers['Content-Type'] = 'application/json';

    $api_headers = $headers;
  }
  else {
    $api_headers = array('Content-Type' => 'application/json',
      'X-Auth-Email' => $cf_settings['cf_api_email'],
      'X-Auth-Key' => $cf_settings['cf_api_key'], "Host" => "api.cloudflare.com",
      "Connection" => "Close");
  }

  if (module_exists('httprl')) {

    if ($data_type == 'json')
      $api_data = json_encode($data);
    elseif ($data_type == 'binary')
      $api_data = json_encode($data);
    else
      $api_data = $data;


// Set options.
    $options = array(
      'method' => $method,
      'data' => $api_data,
      'headers' => $api_headers
    );

// Queue up the request.
    httprl_request($url, $options);

// Execute request.
    $request = httprl_send_request();
// Merge into base array if only one argument passed in.
    if (count($request) == 1) {
      $return = array_pop($request);
    }

    if (is_object($return)) {
      if (strlen(trim($return->data)) > 0) {
        $return = $return->data;
      }
    }
    else
      $return = FALSE;
  }
  else {

    $http_headers = array();
    foreach ($api_headers as $k => $v) {
      $http_headers[] = $k . ':' . $v;
    }

    $opts = array(
      'http' => array(
        'method' => "POST",
        'header' => $http_headers,
        'content' => json_encode($data)
      )
    );
    $context = stream_context_create($opts);

// Open the file using the HTTP headers set above
    $return = file_get_contents($url, FALSE, $context);
  }

  return $return;
}

function cfworkers_api_get_scripts() {
  $account_id = variable_get('cf_account_id');

  $url = "https://api.cloudflare.com/client/v4/accounts/$account_id/workers/scripts";
  $res = cfworkers_api_call($url, '', 'GET');

  if ($res) {
    $result = json_decode($res, TRUE);
    if (isset($result['result'])) {
      return $result['result'];
    }
    else
      return FALSE;
  }
  else
    return FALSE;
}

function cfworkers_api_get_routes() {

  $cf_settings = _cloudflare_settings();
  $cf_zone_id = $cf_settings['cf_zone_id'];

  $url = "https://api.cloudflare.com/client/v4/zones/$cf_zone_id/workers/routes";
  $res = cfworkers_api_call($url, '', 'GET');

  if ($res) {
    $result = json_decode($res, TRUE);
    if (isset($result['result'])) {
      return $result['result'];
    }
    else
      return FALSE;
  }
  else
    return FALSE;
}

function cfworkers_api_upload_script($script_name, $js_data) {
  $account_id = variable_get('cf_account_id');
  $url = "https://api.cloudflare.com/client/v4/accounts/$account_id/workers/scripts/$script_name";
  $headers = array('Content-Type' => 'application/javascript');

  $res = cfworkers_api_call($url, $js_data, 'PUT', $headers);
  $result = json_decode($res, TRUE);


  watchdog('cfworkers_api', "Upload Script " . var_export($result, TRUE));

  if (!isset($result['result'])) {
    $error_msg = '';
    foreach ($result['errors'] as $err) {
      $msg = '';
      $msg = cfworkers_api_get_error_message($err['code']);
      if ($msg == '')
        $msg = $err['code'] . ' ' . $err['message'];

      $error_msg .= $error_msg == '' ? $msg : $msg . ',';
    }

    return $error_msg;
  }
  else
    return $result['result'];
}

function cfworkers_api_download_script($script_name) {
  $account_id = variable_get('cf_account_id');
  $url = "https://api.cloudflare.com/client/v4/accounts/$account_id/workers/scripts/$script_name";
  $headers = array('Content-Type' => 'application/javascript');

  $res = cfworkers_api_call($url, '', 'GET', $headers);
  return $res;
}

function cfworkers_api_delete_script($script_name) {
  $account_id = variable_get('cf_account_id');
  $url = "https://api.cloudflare.com/client/v4/accounts/$account_id/workers/scripts/$script_name";
  $headers = array('Content-Type' => 'application/javascript');

  $res = cfworkers_api_call($url, '', 'DELETE', $headers);
  $result = json_decode($res, TRUE);
  if ($result['success'] == 'TRUE') {
    return $result['result'];
  }
  else {
    $error_msg = '';
    foreach ($result['errors'] as $err) {
      $msg = '';
      $msg = cfworkers_api_get_error_message($err['code']);
      if ($msg == '')
        $msg = $err['code'] . ' ' . $err['message'];

      $error_msg .= $error_msg == '' ? $msg : $msg . ',';
    }

    return $error_msg;
  }
}

/**
 * creates the route with passed pattern and script
 * @param string $pattern URL Pattern for this Route
 * @param string $script_name Name/Id of the cloudflare service worker script
 * @return array/string returns array of route details if its success, else returns the error message received from cloudflare
 */
function cfworkers_api_create_route($pattern, $script_name) {

  $cf_settings = _cloudflare_settings();
  $cf_zone_id = $cf_settings['cf_zone_id'];

  $url = "https://api.cloudflare.com/client/v4/zones/$cf_zone_id/workers/routes";
  $headers = array('Content-Type' => 'application/json');

  $data['pattern'] = $pattern;
  $data['script'] = $script_name;
  $res = cfworkers_api_call($url, $data, 'POST', $headers, 'json');

  $result = json_decode($res, TRUE);
  watchdog('cfworkers_api', "create route " . var_export($result, TRUE));
  if ($result['success'] == 'TRUE') {
    return $result['result'];
  }
  else {

    $error_msg = '';
    foreach ($result['errors'] as $err) {
      $msg = '';
      $msg = cfworkers_api_get_error_message($err['code']);
      if ($msg == '')
        $msg = $err['code'] . ' ' . $err['message'];

      $error_msg .= $error_msg == '' ? $msg : $msg . ',';
    }

    return $error_msg;
  }
}

/**
 * updates the route and returns its details as array if success
 * @param string $pattern URL Pattern for this Route
 * @param string $script_name Name/Id of the cloudflare service worker script
 * @param string $route_id Id of the route which needs to be updated
 * @return array/string returns array of route details if its success, else returns the error message received from cloudflare
 */
function cfworkers_api_update_route($pattern, $script_name, $route_id) {

  $cf_settings = _cloudflare_settings();
  $cf_zone_id = $cf_settings['cf_zone_id'];

  $url = "https://api.cloudflare.com/client/v4/zones/$cf_zone_id/workers/routes/$route_id";
  $headers = array('Content-Type' => 'application/json');

  $data['pattern'] = $pattern;
  $data['script'] = $script_name;
  $res = cfworkers_api_call($url, $data, 'PUT', $headers, 'json');

  $result = json_decode($res, TRUE);
  watchdog('cfworkers_api', "update route " . var_export($result, TRUE));
  if ($result['success'] == 'TRUE') {
    return $result['result'];
  }
  else {
    $error_msg = '';
    foreach ($result['errors'] as $err) {
      $msg = '';
      $msg = cfworkers_api_get_error_message($err['code']);
      if ($msg == '')
        $msg = $err['code'] . ' ' . $err['message'];

      $error_msg .= $error_msg == '' ? $msg : $msg . ',';
    }

    return $error_msg;
  }
}

function cfworkers_api_delete_route($route_id) {

  $cf_settings = _cloudflare_settings();
  $cf_zone_id = $cf_settings['cf_zone_id'];

  $url = "https://api.cloudflare.com/client/v4/zones/$cf_zone_id/workers/routes/$route_id";

  $headers = array('Content-Type' => 'application/json');
  $res = cfworkers_api_call($url, '', 'DELETE', $headers);

  $result = json_decode($res, TRUE);
  watchdog('cfworkers_api', "delete route " . var_export($result, TRUE));
  if ($result['success'] == 'TRUE') {
    return $result['result'];
  }
  else {
    $error_msg = '';
    foreach ($result['errors'] as $err) {
      $msg = '';
      $msg = cfworkers_api_get_error_message($err['code']);
      if ($msg == '')
        $msg = $err['code'] . ' ' . $err['message'];

      $error_msg .= $error_msg == '' ? $msg : $msg . ',';
    }

    return $error_msg;
  }
}

function cfworkers_api_get_error_message($code) {
  $error_msgs = array(
    '10020' => 'Attempted to create a filter for a pattern that already exists',
    '10022' => 'Filter pattern was invalid',
    '10001' => 'Unsupported or unexpected Content Type',
    '10023' => 'Unauthorized access attempt',
    '10002' => 'Unexpected internal server error',
    '10003' => 'Missing required URL parameter',
    '10014' => 'Internal error while attempting authorization checks',
    '10004' => 'Malformed URL parameter',
    '10015' => 'The current account is not authorized to use workers',
    '10026' => 'Filter pattern was unparseable',
    '10007' => 'Resource not found (similar to HTTP 404)'
  );
  if (isset($error_msgs[$code]))
    return $error_msgs[$code];
  else
    return '';
}