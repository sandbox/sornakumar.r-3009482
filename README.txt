-- INTRODUCTION --

CfWorkers modules allows administer of cloudflare service worker scripts
and routes within drupal application

https://developers.cloudflare.com/workers/api/config-api-for-enterprise/.

It uses the APIs provided by cloudflare to manage the worker scripts and 
routes. Currently, its developed as sub module of cloudflare, it uses the
cloudflare zone id and api key information from cloudflare module

-- REQUIREMENTS --

 - Cloudflare API Key 
 - Cloudflare API Zone ID
 - Cloudflare Account ID 

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Configure via path for Drupal7 via /admin/config/people/cfworker
